import axios from "axios";
import { API_URL } from "./../utils/variables";

class TodosService {
	get() {
		return new Promise((res, rej) => {
			return axios
				.get(`${API_URL}todos.json`)
				.then(result => {
					res(result.data);
				})
				.catch(err => {
					console.error(err);
					rej(err);
				});
		});
	}
}

export default new TodosService();
