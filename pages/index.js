import React, { useContext, useEffect } from "react";

import TodosService from "../services/todos.service";
import { todosContext } from "../stores/todos.store";
import Todos from "../modules/todos/todos.module";

export default () => {
	const { dispatch } = useContext(todosContext);
	useEffect(() => {
		const loadData = async () => {
			const items = await TodosService.get();
			dispatch({ type: "INIT", items });
		};
		loadData();
	}, []);
	return <Todos />;
};
