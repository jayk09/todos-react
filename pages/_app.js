import React from "react";
import App from "next/app";
import styledSanitize from "styled-sanitize";
import { createGlobalStyle, ThemeProvider } from "styled-components";

import { TodosProvider } from "../stores/todos.store";
import theme from "../stylesheets/theme";

const GlobalStyle = createGlobalStyle`
${styledSanitize}
@import url('https://fonts.googleapis.com/css?family=Abril+Fatface|Montserrat:400,700&display=swap&subset=latin-ext');
html {
	font-size: 62.5%;
}
body {
	position: relative;
	background-color: ${({ theme }) => theme.COLOR().DARK};
	color: ${({ theme }) => theme.COLOR().LIGHT};
	font-family: "Montserrat", sans-serif;
	font-size: 1.6rem;
}
main {
	width: 100%;
	max-width: 480px;
	margin: 80px auto;
	padding: 0 15px;
	@media screen and (min-width: ${({ theme }) => theme.BREAKPOINT().MD}) {
		padding: 0;
	}
}

h1,
h2,
h3,
h4,
h5,
h6 {
	font-family: "Abril Fatface", cursive;
}
`;

export default class MyApp extends App {
	render() {
		const { Component, pageProps } = this.props;
		return (
			<ThemeProvider theme={theme}>
				<GlobalStyle />
				<TodosProvider>
					<main>
						<Component {...pageProps} />
					</main>
				</TodosProvider>
			</ThemeProvider>
		);
	}
}
