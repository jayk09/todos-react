const BREAKPOINT = (down = false) => ({
	SM: 480 - down + "px",
	MD: 768 - down + "px"
});

const COLOR = (alfa = 1) => ({
	DARK: "rgba(22, 29, 34, " + alfa + ")",
	LIGHT: "rgba(245, 245, 245, " + alfa + ")",
	LIGHT: "rgba(245, 245, 245, " + alfa + ")",
	SUCCESS: "rgba(107, 209, 158, " + alfa + ")"
});

export default {
	BREAKPOINT,
	COLOR
};
