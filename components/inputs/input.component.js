import React from "react";
import styled from "styled-components";

const Input = styled.input`
	outline: none;
	font-size: 1.6rem;
	border: none;
	border-radius: 4px;
	width: 100%;
	margin: 0 0 25px;
	padding: 20px;
	opacity: 0.5;
	transition: opacity 0.3s;
	&:focus {
		opacity: 1;
	}
`;

export default props => <Input {...props} />;
