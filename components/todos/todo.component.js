import React, { useContext } from "react";
import styled, { css } from "styled-components";

import { todosContext } from "../../stores/todos.store";

const Todo = styled.label`
	top: 0;
	left: 0;
	display: flex;
	align-items: center;
	justify-content: flex-start;
	font-size: 1.6rem;
	line-height: 1;
	padding: 15px;
	cursor: pointer;
	margin-bottom: 10px;
	border-radius: 2px;
	user-select: none;
	color: ${({ theme }) => theme.COLOR().DARK};
	transition: background-color 0.3s;
	&.item-enter-active {
		opacity: 1;
		transform: translateY(0%);
		transition: opacity 1s linear, transform 1s linear;
	}
	&.item-exit {
		transform: translateY(0%);
		opacity: 1;
	}
	${({ isDone }) =>
		isDone
			? css`
					&.item-enter {
						transform: translateY(-400%);
						opacity: 0;
					}
					&.item-exit-active {
						transform: translateY(-400%);
						opacity: 0;
						transition: opacity 1s linear, transform 1s linear;
					}
					background-color: ${({ theme }) => theme.COLOR().SUCCESS};
					&:hover {
						background-color: ${({ theme }) => theme.COLOR(0.75).SUCCESS};
					}
			  `
			: css`
					&.item-enter {
						transform: translateY(400%);
						opacity: 0;
					}
					&.item-exit-active {
						transform: translateY(400%);
						opacity: 0;
						transition: opacity 1s linear, transform 1s linear;
					}
					background-color: ${({ theme }) => theme.COLOR().LIGHT};
					&:hover {
						background-color: ${({ theme }) => theme.COLOR(0.75).LIGHT};
					}
			  `}
	&:hover {
		background-color: ${({ theme }) => theme.COLOR(0.75).LIGHT};
		button {
			opacity: 1;
		}
	}

	span {
		width: 100%;
	}
	input {
		margin-right: 10px;
	}
	button {
		position: relative;
		float: right;
		height: 20px;
		width: 20px;
		margin-left: 10px;
		box-sizing: border-box;
		line-height: 1;
		cursor: pointer;
		background-color: ${({ theme }) => theme.COLOR().DARK};
		border-radius: 50rem;
		border: none;
		outline: none;
		transition: opacity 0.3s;
		color: ${({ theme }) => theme.COLOR().LIGHT};
		opacity: 0;
	}
`;

export default item => {
	const { dispatch } = useContext(todosContext);
	return (
		<Todo isDone={item.done}>
			<input
				type="checkbox"
				checked={item.done}
				onChange={() =>
					dispatch({ type: "UPDATE", item: { ...item, done: !item.done } })
				}
			/>
			<span>{item.title}</span>
			<button onClick={() => dispatch({ type: "REMOVE", item })}>x</button>
		</Todo>
	);
};
