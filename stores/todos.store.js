import React, { createContext, useReducer } from "react";

const initialState = [];
const todosContext = createContext(initialState);
const { Provider } = todosContext;

let uid = 0;
const reducer = (state, action) => {
	switch (action.type) {
		case "INIT":
			const { items } = action;
			uid = items.length;
			return [...items];
		case "ADD":
			const { input } = action;
			const item = {
				id: ++uid,
				title: input,
				done: false
			};
			return [item, ...state];
		case "REMOVE":
			return [...state.filter(i => i.id !== action.item.id)];
		case "UPDATE":
			return [action.item, ...state.filter(i => i.id !== action.item.id)];
		default:
			throw new Error("Invalid dispatch type: " + action.type);
	}
};

const TodosProvider = ({ children }) => {
	const [todos, dispatch] = useReducer(reducer, initialState);
	return <Provider value={{ todos, dispatch }}>{children}</Provider>;
};

export { todosContext, TodosProvider };
