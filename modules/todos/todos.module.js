import React, { Fragment, useState, useContext } from "react";

import { CSSTransition, TransitionGroup } from "react-transition-group";

import { todosContext } from "../../stores/todos.store";
import TodoItem from "../../components/todos/todo.component";
import Input from "../../components/inputs/input.component";

export default () => {
	const { dispatch, todos } = useContext(todosContext);
	const [input, setInput] = useState("");
	const addItem = e => {
		e.preventDefault();
		if (input) {
			dispatch({ type: "ADD", input });
			setInput("");
		}
	};

	const todoItems = todos.filter(i => !i.done);
	const doneItems = todos.filter(i => i.done);

	return (
		<Fragment>
			<h1>Jak získat práci:</h1>
			<form onSubmit={addItem}>
				<Input
					placeholder="Přidejte úkol..."
					value={input}
					onChange={e => setInput(e.target.value)}
				/>
			</form>
			{!todos.length ? (
				<h2>Nechtělo by to nějaké úkoly?</h2>
			) : (
				<Fragment>
					{todoItems.length ? (
						<p>Zbývá dokončit:</p>
					) : (
						<h2>Hurá! Vše je hotovo.</h2>
					)}
					<TransitionGroup component={null}>
						{todoItems.map(item => (
							<CSSTransition key={item.id} timeout={250} classNames="item">
								<TodoItem {...item} />
							</CSSTransition>
						))}
					</TransitionGroup>
					<hr />
					{doneItems.length ? (
						<p>Hotovo:</p>
					) : (
						<h2>Ještě toho zbývá tolik udělat...</h2>
					)}
					<TransitionGroup component={null}>
						{doneItems.map(item => (
							<CSSTransition key={item.id} timeout={250} classNames="item">
								<TodoItem {...item} />
							</CSSTransition>
						))}
					</TransitionGroup>
				</Fragment>
			)}
		</Fragment>
	);
};
